<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<refentry id='osgearth_package'>

  <refmeta>
    <refentrytitle>osgearth_package</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>osgearth_package</refname>
    <refpurpose>create a redistributable TMS based package</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>osgearth_package</command>
      <group>
        <arg choice='opt'><option>--tms</option></arg>
        <arg choice='opt'><option>--out</option> <replaceable>path</replaceable></arg>
        <arg choice='opt'><option>--bounds</option> <replaceable>xmin</replaceable> <replaceable>ymin</replaceable> <replaceable>xmax</replaceable> <replaceable>ymax</replaceable></arg>
        <arg choice='opt'><option>--max-level</option> <replaceable>level</replaceable></arg>
        <arg choice='opt'><option>--out-earth</option> <replaceable>earthfile</replaceable></arg>
        <arg choice='opt'><option>--ext</option> <replaceable>extension</replaceable></arg>
        <arg choice='opt'><option>--overwrite</option></arg>
        <arg choice='opt'><option>--keep-empties</option></arg>
        <arg choice='opt'><option>--continue-single-color</option></arg>
        <arg choice='opt'><option>--db-options</option> <replaceable>options</replaceable></arg>
      </group>
      <arg choice='plain'><replaceable>earthfile.earth</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <command>osgearth_package</command> creates a redistributable 
      <ulink url="http://en.wikipedia.org/wiki/Tile_Map_Service">TMS</ulink>
      based package from an earth file.
    </para>
  </refsect1>

  <refsect1 id='options'>
    <title>OPTIONS</title>
    <variablelist>

      <varlistentry>
        <term><option>--tms</option></term>
        <listitem>
          <para>make a TMS repo</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--out</option> <replaceable>path</replaceable></term>
        <listitem>
          <para>root output folder of the TMS repo (required)</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--bounds</option> <replaceable>xmin</replaceable> <replaceable>ymin</replaceable> <replaceable>xmax</replaceable> <replaceable>ymax</replaceable></term>
        <listitem>
          <para>
            bounds to package (in map coordinates; default=entire map)
            You can provide multiple bounds
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--max-level</option> <replaceable>level</replaceable></term>
        <listitem>
          <para>max LOD level for tiles (all layers; default=inf)</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--out-earth</option> <replaceable>earthfile</replaceable></term>
        <listitem>
          <para>export an earth file referencing the new repo</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--ext</option> <replaceable>extension</replaceable></term>
        <listitem>
          <para>overrides the image file extension (e.g. jpg)</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--overwrite</option></term>
        <listitem>
          <para>overwrite existing tiles</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--keep-empties</option></term>
        <listitem>
          <para>writes out fully transparent image tiles (normally discarded)</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--continue-single-color</option></term>
        <listitem>
          <para>
            continues to subdivide single color tiles, subdivision typically
            stops on single color images
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--db-options</option> <replaceable>options</replaceable></term>
        <listitem>
          <para>
            db options string to pass to the image writer in quotes
            (e.g., "JPEG_QUALITY 60")
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><replaceable>file.earth</replaceable></term>
        <listitem>
          <para>Path to the earth file</para>
        </listitem>
      </varlistentry>

    </variablelist>

  </refsect1>

</refentry>
