<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<refentry id='osgearth_cache'>

  <refmeta>
    <refentrytitle>osgearth_cache</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>osgearth_cache</refname>
    <refpurpose>manage osgEarth's cache</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>osgearth_cache</command>
      <group>
        <arg choice='opt'><option>--list</option></arg>
        <arg choice='opt'><option>--seed</option></arg>
        <arg choice='opt'><option>--estimate</option></arg>
        <arg choice='opt'><option>--threads</option></arg>
        <arg choice='opt'><option>--min-level</option> <replaceable>level</replaceable></arg>
        <arg choice='opt'><option>--max-level</option> <replaceable>level</replaceable></arg>
        <arg choice='opt'><option>--bounds</option> <replaceable>xmin</replaceable> <replaceable>ymin</replaceable> <replaceable>xmax</replaceable> <replaceable>ymax</replaceable></arg>
        <arg choice='opt'><option>--index</option> <replaceable>shapefile</replaceable></arg>
        <arg choice='opt'><option>--cache-path</option> <replaceable>path</replaceable></arg>
        <arg choice='opt'><option>--cache-type</option> <replaceable>type</replaceable></arg>
        <arg choice='opt'><option>--purge</option></arg>
      </group>
      <arg choice='plain'><replaceable>file.earth</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <command>osgearth_cache</command> can be used to manage osgEarth's cache.
      The most common usage of <command>osgearth_cache</command> is to populate
      a cache in a non-interactive manner using the <option>--seed</option>
      argument. See
      <ulink url="http://docs.osgearth.org/en/latest/user/caching.html">Caching</ulink>
      for more information on caching.
    </para>
  </refsect1>

  <refsect1 id='options'>
    <title>OPTIONS</title>
    <variablelist>

      <varlistentry>
        <term><option>--list</option></term>
        <listitem>
          <para>Lists info about the cache in a .earth file</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--seed</option></term>
        <listitem>
          <para>Seeds the cache in a .earth file</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--estimate</option></term>
        <listitem>
          <para>Print out an estimation of the number of tiles, disk space and time it will take to perform this seed operation</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--threads</option></term>
        <listitem>
          <para>The number of threads to use for the seed operation (default=1)</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--min-level</option> <replaceable>level</replaceable></term>
        <listitem>
          <para>Lowest LOD level to seed (default=0)</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--max-level</option> <replaceable>level</replaceable></term>
        <listitem>
          <para>Highest LOD level to seed (default=highest available)</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--bounds</option> <replaceable>xmin</replaceable> <replaceable>ymin</replaceable> <replaceable>xmax</replaceable> <replaceable>ymax</replaceable></term>
        <listitem>
          <para>Geospatial bounding box to seed (in map coordinates; default=entire map)</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--index</option> <replaceable>shapefile</replaceable></term>
        <listitem>
          <para>
            Loads a shapefile (.shp) and uses the feature extents to set the
            cache seeding bounding box(es). For each feature in the shapefile,
            adds a bounding box (similar to <option>--bounds</option>) to
            constrain the region you wish to cache.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--cache-path</option> <replaceable>path</replaceable></term>
        <listitem>
          <para>Overrides the cache path in the .earth file</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--cache-type</option> <replaceable>type</replaceable></term>
        <listitem>
          <para>Overrides the cache type in the .earth file</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--purge</option></term>
        <listitem>
          <para>Purges a layer cache in a .earth file</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><replaceable>file.earth</replaceable></term>
        <listitem>
          <para>Path to the earth file</para>
        </listitem>
      </varlistentry>

    </variablelist>

  </refsect1>

</refentry>
